# Cowin Notifier
A python package to setup slack cowin notification whenever there are slots available for vaccination.

## Usage
1. How to setup this on my machine?
    - In your home directory: `git clone https://srp201201051@bitbucket.org/srp201201051/cowin.git`
    - `cd CoWIN`
    - Update `cowin_notifier/constants.py` to provide configuration inputs.
    - Run `python3 setup.py install`
    - Should generate an executable: `check_cowin_availability`
    - Add cron jobs to run the executable every X seconds using something like below:
        - `* * * * * check_cowin_availability >> ~/logs/cowin_availability.log 2>&1`
        - `* * * * * ( sleep 15 ; check_cowin_availability >> ~/logs/cowin_availability.log 2>&1 )`
        - `* * * * * ( sleep 30 ; check_cowin_availability >> ~/logs/cowin_availability.log 2>&1 )`
        - `* * * * * ( sleep 45 ; check_cowin_availability >> ~/logs/cowin_availability.log 2>&1 )`
        - `0 0 * * * echo "" > ~/logs/cowin_availability.log`
    - You will need a log directory to store the logs generate by above process.

2. How to make code changes?
    - Make any code changes as per your need.
    - Run `python3 setup.py install` to update the executatble with the changes you recently made.

3. I see too many `Unauthenticated access!` errors in the logs, what do I do?
    - Usually when the script is trying to access an authorized endpoint of CoWIN, after some time the authorization token stops working, or fails more frequently.
    - The only way around this is changing the `COWIN_AUTH_HEADER` in `constants.py` frequently OR using the public endpoints by setting `USE_COWIN_PUBLIC_ENDPOINT` to True and react on stale data!!
