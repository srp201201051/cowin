import os
import sys
import traceback
import json
import logging
from .constants import LOCATIONS_TO_USERS, COWIN_AUTH_HEADER, DATA_DIR

logger = logging
logging.basicConfig(
    format='%(asctime)s %(levelname)s %(message)s', 
    level=logging.INFO
)

def _format_stacktrace():
    parts = ["Traceback (most recent call last):\n"]
    parts.extend(traceback.format_stack(limit=25)[:-2])
    parts.extend(traceback.format_exception(*sys.exc_info())[1:])
    return "".join(parts)


def execution_metadata(func):
    def wrapper():
        logger.info("Started execution with PID: %d", os.getpid())
        try:
            func()
            logger.info("Exit status: Succeeded.")
        except Exception as e:
            logger.error("Unexpected error: %s", str(e))
            stacktrace = _format_stacktrace()
            logger.error("%s", stacktrace)
            logger.error("Exit status: Failed.")
    return wrapper


def construct_session_key(center, session):
    return f"{center['state_name']}:{center['district_name']}:{center['center_id']}" \
        f":{session['date']}:{session['min_age_limit']}:{session['vaccine']}"


def get_last_fetched_availability_data():
    jdata = {}
    fname = DATA_DIR + "availability.json"
    if not os.path.isfile(fname):
        with open(fname, 'w') as f:
            f.write("{}\n")

    with open(fname, 'r') as f:
        data = f.read()
        jdata = json.loads(data)
    return jdata


def persist_fetched_availability_data(availability_data):
    fname = DATA_DIR + "availability.json"
    json.dump(availability_data, open(fname, 'w'), indent=4, sort_keys=True)


def get_location_ids_mapping():
    jdata = {}
    fname = DATA_DIR + "locations_ids.json"
    if not os.path.isfile(fname):
        with open(fname, 'w') as f:
            f.write("{}\n")

    with open(fname, 'r') as f:
        data = f.read()
        jdata = json.loads(data)
    return jdata    


def persist_location_ids_mapping(locations_ids):
    fname = DATA_DIR + "locations_ids.json"
    json.dump(locations_ids, open(fname, 'w'), indent=4, sort_keys=True)


def construct_slack_message(center, session, users):
    message = ""
    for username in users:
        message += f"@{username} "
    message += "\n"
    message += "*:rocket: CoWIN Availability found!! :rocket:*\n"
    message += "\n"
    message += f"*Location: `{center['state_name']} - {center['district_name']}`*\n"
    message += f"*Date: {session['date']}*\n"
    message += f"*Center: {center['name']} - {center['address']}*\n"
    message += f"*Vaccine: {session['vaccine']}*\n"
    message += f"*Available Capacity: {session['available_capacity']}*\n"
    message += f"*Book now:* https://www.cowin.gov.in/home\n"
    return message


def get_cowin_headers():
    headers = {}
    headers['accept'] = 'application/json, text/plain, */*'
    headers['authorization'] = COWIN_AUTH_HEADER
    headers['origin'] = 'https://selfregistration.cowin.gov.in'
    headers['referer'] = 'https://selfregistration.cowin.gov.in/'
    headers['accept-encoding'] = 'gzip, deflate, br'
    headers['accept-language'] = 'en-US,en;q=0.9,hi;q=0.8,gu;q=0.7'
    headers['user-agent'] = 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.93 Safari/537.36'
    headers['sec-ch-ua'] =  '" Not A;Brand";v="99", "Chromium";v="90", "Google Chrome";v="90"'
    headers['sec-fetch-dest'] =  'empty'
    headers['sec-fetch-mode'] =  'cors'
    headers['sec-fetch-site'] =  'cross-site'
    headers['sec-ch-ua-mobile'] = '?0'
    return headers
