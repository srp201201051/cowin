import json
import time
import requests
from .utils import logger
from .constants import SLACK_WEBHOOK

class SlackNotifier:
    def __init__ (self):
        self.webhook_url = SLACK_WEBHOOK
        self.headers = {'Content-Type': 'application/json'}
        self.max_retries = 3

    def _send_notification_helper(self, message):
        message_data = {'text': message}
        response = requests.post(self.webhook_url, data=json.dumps(message_data), headers=self.headers)
        return response

    def send_notification(self, message):
        retries = 0
        while retries < self.max_retries:
            response = self._send_notification_helper(message)
            if response.status_code == 200:
                return response
            logger.error(f"Error sending slack notification: {response.content}.")
            retries += 1
            time.sleep(2.0)
        raise Exception("Could not send slack notification")
