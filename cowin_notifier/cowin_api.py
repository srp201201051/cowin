import json
import time
import requests
from datetime import datetime
from .utils import get_cowin_headers, logger
from .constants import USE_COWIN_PUBLIC_ENDPOINT

class CoWIN:
    def __init__ (self):
        self.headers = get_cowin_headers()
        self.max_retries = 3

    def _fetch_data(self, data_url):
        retries = 0
        while retries < self.max_retries:
            response = requests.get(data_url, headers=self.headers)
            if response.status_code == 200:
                time.sleep(0.5)
                return response
            logger.error(f"Error fetching CoWIN availability: {response.content}")
            retries += 1
            time.sleep(2.0)
        raise Exception("Could not fetch availability.")

    def fetch_availability_with_location_id(self, location_id):
        date = datetime.now().strftime("%d-%m-%Y")
        data_url = f"https://cdn-api.co-vin.in/api/v2/appointment/sessions/" \
                            f"calendarByDistrict?district_id={location_id}&date={date}"
        if USE_COWIN_PUBLIC_ENDPOINT:
            data_url = f"https://cdn-api.co-vin.in/api/v2/appointment/sessions/" \
                                f"public/calendarByDistrict?district_id={location_id}&date={date}"
        response = self._fetch_data(data_url)
        return json.loads(response.content.decode("utf-8"))

    def fetch_availability_with_pincode(self, pincode):
        date = datetime.now().strftime("%d-%m-%Y")
        data_url = f"https://cdn-api.co-vin.in/api/v2/appointment/sessions/" \
                            f"calendarByPin?pincode={pincode}&date={date}"
        if USE_COWIN_PUBLIC_ENDPOINT:
            data_url = f"https://cdn-api.co-vin.in/api/v2/appointment/sessions/" \
                                f"public/calendarByPin?pincode={pincode}&date={date}"
        response = self._fetch_data(data_url)
        return json.loads(response.content.decode("utf-8"))

    def fetch_states_data(self):
        data_url = "https://cdn-api.co-vin.in/api/v2/admin/location/states"
        response = self._fetch_data(data_url)
        data = json.loads(response.content.decode("utf-8"))
        state_to_id = {}
        for state_info in data['states']:
            state_to_id[state_info['state_name']] = state_info['state_id']
        return state_to_id

    def fetch_districts_data(self, state_id):
        data_url = f"https://cdn-api.co-vin.in/api/v2/admin/location/districts/{state_id}"
        response = self._fetch_data(data_url)
        data = json.loads(response.content.decode("utf-8"))
        district_to_id = {}
        for district_info in data['districts']:
            district_to_id[district_info['district_name']] = district_info['district_id']
        return district_to_id
