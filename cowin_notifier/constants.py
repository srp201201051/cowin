from os.path import expanduser
from string import Template

"""
The app stores a couple of files to maintain state across multiple executions. By default, it tries
to use "~/CoWIN/cowin_notifier/data/" directory, and if you want to use a different directory, please
provide the absolute path of the directory with a slash at the end.
"""
DATA_DIR = expanduser("~") + "/CoWIN/cowin_notifier/data/"


"""
One time setup for a slack channel:
- How to: https://slack.com/intl/en-in/help/articles/115005265063-Incoming-webhooks-for-Slack
- You need to create a slack app, and possible a slack workspace and a slack channel.
- After setting it up, just put the webhook url in below vairbale.

Example of how it looks(given example url is an invalid one):
SLACK_WEBHOOK = "https://hooks.slack.com/services/T030F4J43QW/AFSDF40WDSA3/RfvsddTSdfsdfSDDG234D"
"""
SLACK_WEBHOOK = ""


"""
Decides whether to use a public endpoint or an authorized endpoint of cowin. In case of an authorized
endpoint, please provide a valid value for COWIN_AUTH_HEADER. The public endpoint data is cached, and 
usually outdated. With authorized endpoint, it is essential to keep updating COWIN_AUTH_HEADER value 
if you see too many failures.
"""
USE_COWIN_PUBLIC_ENDPOINT = False


"""
The "authorization" request header value after logging in the cowin platform. It usually works for days, but
better to keep updating this at least once a day. The public endpoints data is cached, and outdated, hence
using the endpoint for authorized users for up-to-date information.
- How to find this value?
    - Go to https://selfregistration.cowin.gov.in/
    - Do right click, and click "Inspect" on the web page, and check "Network" tab.
    - Login using OTP.
    - Check one of the requests that were made after the login.
    - In the "Request Headers" for that request, it should have an "authorization" field.
    - The value of the field should be updated here.

Example of how it looks(given example value is an invalid one):
COWIN_AUTH_HEADER = "Bearer eysdfbkabfskdfadfadADSDFasdadnjasdasdASD.adafsdnfsjnfsADfsdfd.DFSDFSFDsgnskfjg.sdfSDFSDFd.sdfsDFSDF.sdfsfd"
"""
COWIN_AUTH_HEADER = ""


"""
Possible values = [18], [45], [18, 45]
"""
AGE_LIMITS = [18]


"""
Format: LOCATIONS_TO_USERS["<state>:<district>"] = ["<username1>, <username2>"]
- User name can be "here" as well to notify the whole channel
Example:
    LOCATIONS_TO_USERS["Andhra Pradesh:Visakhapatnam"] = ["John Doe"]
    LOCATIONS_TO_USERS["Gujarat:Ahmedabad"] = ["here"]
"""
LOCATIONS_TO_USERS = {}


"""
You can use pincodes as well to provide locations.
Example:
    PINCODES_TO_USERS["721139"] = ["here"]
"""
PINCODES_TO_USERS = {}
