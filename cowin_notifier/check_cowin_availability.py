from .utils import execution_metadata, logger
from .utils import get_location_ids_mapping, persist_location_ids_mapping
from .utils import get_last_fetched_availability_data, persist_fetched_availability_data
from .utils import construct_session_key, construct_slack_message
from .constants import LOCATIONS_TO_USERS, AGE_LIMITS, PINCODES_TO_USERS
from .cowin_api import CoWIN
from .slack_notifier import SlackNotifier
from datetime import datetime


def update_and_get_location_ids():
    today = datetime.now().strftime("%Y%m%d")
    location_ids = get_location_ids_mapping()
    cowin_client = CoWIN()
    state_to_ids = None
    districts_data = {}
    for location_key in LOCATIONS_TO_USERS:
        [state, district] = location_key.split(":")
        if (location_key not in location_ids) or (location_ids[location_key]['last_updated_at'] != today):
            location_ids[location_key] = {}
            if state_to_ids == None:
                state_to_ids = cowin_client.fetch_states_data()
            if state not in districts_data:
                districts_data[state] = cowin_client.fetch_districts_data(state_to_ids[state])
            location_ids[location_key]['location_id'] = districts_data[state][district]
            location_ids[location_key]['last_updated_at'] = today
    persist_location_ids_mapping(location_ids)
    return location_ids


    # state_to_ids = cowin_client.fetch_states_data()
    districts_data = {}
    for location_key in LOCATIONS_TO_USERS:
        [state, district] = location_key.split(":")
        if state not in districts_data:
            districts_data[state] = cowin_client.fetch_districts_data(state_to_ids[state])
        location_ids[location_key] = districts_data[state][district]
    return location_ids


def should_notify(center, session, availability_data):
    curr_ts_str = datetime.now().strftime("%Y-%m-%dT%H:%M:%S")
    session_key = construct_session_key(center, session)
    if session_key not in availability_data:
        availability_data[session_key] = {}
        availability_data[session_key]['availability'] = 0
        availability_data[session_key]['last_updated_at'] = curr_ts_str

    prev_availability = availability_data[session_key]['availability']
    availability_data[session_key]['availability'] = session['available_capacity']
    availability_data[session_key]['last_updated_at'] = curr_ts_str

    # Notify only when availability goes non-zero from zero to avoid spurious notifications
    if prev_availability == 0 and session['available_capacity'] > 0:
        return True
    return False


def process_cowin_data(cowin_data, availability_data, users):
    slack_cient = SlackNotifier()
    for center in cowin_data['centers']:
        for session in center['sessions']:
            if session['min_age_limit'] in AGE_LIMITS:
                if should_notify(center, session, availability_data):
                    message = construct_slack_message(center, session, users)
                    slack_cient.send_notification(message)


@execution_metadata
def main():
    logger.info("Checking cowin availability...")
    availability_data = get_last_fetched_availability_data()
    try:
        cowin_client = CoWIN()

        location_ids = update_and_get_location_ids()
        for location_key in location_ids:
            location_id = location_ids[location_key]['location_id']
            users = LOCATIONS_TO_USERS[location_key]
            cowin_data = cowin_client.fetch_availability_with_location_id(location_id)
            process_cowin_data(cowin_data, availability_data, users)

        for pincode in PINCODES_TO_USERS:
            users = PINCODES_TO_USERS[pincode]
            cowin_data = cowin_client.fetch_availability_with_pincode(pincode)
            process_cowin_data(cowin_data, availability_data, users)
    finally:
        persist_fetched_availability_data(availability_data)
