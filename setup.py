# Copyright 2020 StratFactory.com, Inc. or its affiliates. All Rights Reserved.

from setuptools import setup, find_packages

VERSION = '0.0.1'
DESCRIPTION = "A package to notify about vaccine availability on slack"

REQUIRED_PACKAGES = [
    "requests"
]

setup(
    name='CoWIN Notifier',
    version=VERSION,
    packages=find_packages(),
    description=DESCRIPTION,
    long_description=DESCRIPTION,
    long_description_content_type="text/markdown",
    install_requires=REQUIRED_PACKAGES,
    entry_points = {
        'console_scripts': [
            'check_cowin_availability=cowin_notifier.check_cowin_availability:main'
        ]
    },
    classifiers=[
        "Programming Language :: Python :: 3"
    ],
    python_requires='>=3.7',
    include_package_data=True,
)
